# Matdun CV Task
## Task

Need to create a real time face recogniton module using state-of-the-art computer vision model. 

We expect you to:
-> Perform basic image processing on the training and testing data.
-> Explain the architecture and loss function of the used model.
-> Calculating the fps of the model.


## Files

- outpy2.avi - testing video on which face recogniton needs to be performed.
- train_subjects - directory contains different subjects need to be enrolled in this FR task.

## Submission

Deadline for the submission is 19 November 2021 11:59 PM. Please don't put the assignment solution on git as a public repository. Please send it to me on ashish.arora@matdun.com.